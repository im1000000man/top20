package com.wes.top20;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.wes.top20.model.Application;

public final class Top20DetailViewFragment extends Fragment {

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		
		final View v = inflater.inflate(R.layout.top20_details, container, false);
		final Application app = ((Application) (this.getArguments().getParcelable("$Top20.app")));
		
		// Update view status with application data
		((ImageView) (v.findViewById(R.id.app_icon))).setImageResource(app.getAppIconReference());
		((TextView) (v.findViewById(R.id.title))).setText(app.getTitle());
		((TextView) (v.findViewById(R.id.downloads))).setText(app.getDownloads() + " downloads");
		((CheckBox) (v.findViewById(R.id.safe_for_work))).setChecked(app.isSafeForWork());
		
		((TextView) (v.findViewById(R.id.description))).setText(app.getDescription());
		
		final Button download = ((Button) (v.findViewById(R.id.download)));
		download.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				
				final Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(Top20DetailViewFragment.this.getString(app.getLinkToApp())));
				
				Top20DetailViewFragment.this.startActivity(i);
			}
			
		});
		final Button share = ((Button) (v.findViewById(R.id.share)));
		share.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				
				final Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(Intent.EXTRA_TEXT, app.toString());
				i.setType("test/plain");
				
				Top20DetailViewFragment.this.startActivity(i);
			}
			
		});
		
		return v;
	}
	
	
	
}
