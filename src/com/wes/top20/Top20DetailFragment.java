package com.wes.top20;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.wes.top20.model.ApplicationArray;

public class Top20DetailFragment extends Fragment {
	
	private final static class Top20DetailsAdapter extends FragmentPagerAdapter {
		
		private ApplicationArray array;
		
		public Top20DetailsAdapter(final FragmentManager fm) {
			
			super(fm);
			this.array = ApplicationArray.getArray();
		}

		@Override
		public Fragment getItem(final int position) {
			
			final Fragment f = new Top20DetailViewFragment();
			
			final Bundle b = new Bundle();
			b.putParcelable("$Top20.app", this.array.getApp(position));
			f.setArguments(b);
			
			return f;
		}
		@Override
		public int getCount() {
			return 20;
		}
		
	}
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		
		final int start_position = this.getArguments().getInt("start_position");
		final ViewPager v = new ViewPager(this.getActivity());
		
		v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		v.setId(231342);
		v.setAdapter(new Top20DetailsAdapter(this.getFragmentManager()));
		v.setCurrentItem(start_position);
		v.setBackgroundResource(android.R.drawable.screen_background_dark);
		
		return v;
	}

}
