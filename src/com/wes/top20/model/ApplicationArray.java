package com.wes.top20.model;




import java.util.Arrays;
import java.util.Comparator;

import com.wes.top20.R;

/** A wrapper class containing all the models data. */
public class ApplicationArray {
	
	private final static ApplicationArray APP_ARRAY = new ApplicationArray();
	private Application[] apps;
	
	private ApplicationArray() {
		
		this.apps = new Application[20];
		this.apps[0] = new Application(0, R.drawable.angry_birds_go, R.string.name_angry_birds_go, R.string.description_angry_birds_go, R.string.address_angry_birds_go, 500000000);
		this.apps[1] = new Application(0, R.drawable.angry_birds, R.string.name_angry_birds, R.string.description_angry_birds, R.string.address_angry_birds, 100000000);
		this.apps[2] = new Application(0, R.drawable.anomaly_2, R.string.name_anomaly_2, R.string.description_anomaly_2, R.string.address_anomaly_2, 50000);
		this.apps[3] = new Application(0, R.drawable.asphalt_8, R.string.name_asphalt_8, R.string.description_asphalt_8, R.string.address_asphalt_8, 10000000);
		this.apps[4] = new Application(0, R.drawable.blood_n_glory, R.string.name_blood_n_glory, R.string.description_blood_n_glory, R.string.address_blood_n_glory, 50000);
		this.apps[5] = new Application(0, R.drawable.dead_trigger_2, R.string.name_dead_trigger_2, R.string.description_dead_trigger_2, R.string.address_dead_trigger_2, 500000);
		this.apps[6] = new Application(0, R.drawable.despicable_me, R.string.name_despicable_me, R.string.description_despicable_me, R.string.address_despicable_me, 100000000);
		this.apps[7] = new Application(0, R.drawable.drastic_emu, R.string.name_drastic_emu, R.string.description_drastic_emu, R.string.address_drastic_emu, 20000);
		this.apps[8] = new Application(0, R.drawable.mupen64, R.string.name_mupen_64_plus_ae, R.string.description_mupen_64_plus_ae, R.string.address_mupen_64_plus_ae, 50000);
		this.apps[9] = new Application(0, R.drawable.plantsvszombies2, R.string.name_plants_vs_zombies_2, R.string.description_plants_vs_zombies_2, R.string.address_plants_vs_zombies_2, 10000000);
		this.apps[10] = new Application(0, R.drawable.ppssppiconhd, R.string.name_ppsspp, R.string.description_ppsspp, R.string.address_ppsspp, 100000);
		this.apps[11] = new Application(0, R.drawable.rayman_fiesta_run, R.string.name_rayman_fiesta_run, R.string.description_rayman_fiesta_run, R.string.address_rayman_fiesta_run, 300000);
		this.apps[12] = new Application(0, R.drawable.real_racing, R.string.name_real_racing_3, R.string.description_real_racing_3, R.string.address_real_racing_3, 10000000);
		this.apps[13] = new Application(0, R.drawable.retro_arch, R.string.name_retro_arch, R.string.description_retro_arch, R.string.address_retro_arch, 80000);
		this.apps[14] = new Application(0, R.drawable.shadowgun_deadzone, R.string.name_shadowgun_deadzone, R.string.description_shadowgun_deadzone, R.string.address_shadowgun_deadzone, 200000000);
		this.apps[15] = new Application(0, R.drawable.star_warfare, R.string.name_star_warfare, R.string.description_star_warfare, R.string.address_star_warfare, 1000000);
		this.apps[16] = new Application(0, R.drawable.strike_team, R.string.name_strike_team, R.string.description_strike_team, R.string.address_strike_team, 50000000);
		this.apps[17] = new Application(0, R.drawable.tetris, R.string.name_tetris, R.string.description_tetris, R.string.address_tetris, 30000000);
		this.apps[18] = new Application(0, R.drawable.the_sims, R.string.name_the_sims, R.string.description_the_sims, R.string.description_the_sims, 50000000);
		this.apps[19] = new Application(0, R.drawable.candy_crush_logo , R.string.name_candy_crush_saga, R.string.description_candy_crush_saga, R.string.address_candy_crush_saga, 10000000);
		
		Arrays.sort(apps);
	}
	
	public Application getApp(final int position) {
		
		try { return this.apps[position]; }
		catch (final ArrayIndexOutOfBoundsException e) { return null; }
	}
	
	public final static ApplicationArray getArray() {
		
		return ApplicationArray.APP_ARRAY;
	}
	
}
