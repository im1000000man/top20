package com.wes.top20.model;

import android.os.Parcel;
import android.os.Parcelable;

/** Represents a game application in the Google Play Store. */
public class Application implements Comparable<Application>, Parcelable {
	
	public final static Creator<Application> CREATOR = new Creator<Application>() {

		@Override
		public Application createFromParcel(final Parcel p) {
			return new Application(p);
		}
		@Override
		public Application[] newArray(final int size) {
			return new Application[size];
		}
		
	};
	
	private boolean safe_for_work;
	private long downloads;
	private int id, app_icon, description, title, link_to_app;
	
	private Application(final Parcel p) {

		this.id = p.readInt();
		this.app_icon = p.readInt();
		this.title = p.readInt();
		this.description = p.readInt();
		this.safe_for_work = (p.readByte() == 0x01);
		this.downloads = p.readLong();
		this.link_to_app = p.readInt();
	}
	public Application(final int id, final int app_icon, final int title, final int description, final int link_to_app, long downloads) {
		this(id, app_icon, title, description, link_to_app, true, downloads);
	}
	public Application(final int id, final int app_icon, final int title, final int description, final int link_to_app, final boolean safe_for_work, final long downloads) {
		
		this.id = id;
		this.app_icon = app_icon;
		this.title = title;
		this.description = description;
		this.link_to_app = link_to_app;
		this.safe_for_work = safe_for_work;
		this.downloads = downloads;
	}
	
	public int getTitle() {
		return this.title;
	}
	public int getDescription() {
		return this.description;
	}
	public int getLinkToApp() {
		return this.link_to_app;
	}
	public boolean isSafeForWork() {
		return this.safe_for_work;
	}
	public long getDownloads() {
		return this.downloads;
	}
	public int getID() {
		return this.id;
	}
	public int getAppIconReference() {
		return this.app_icon;
	}
	
	@Override
	public int describeContents() {
		return this.id;
	}
	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		
		dest.writeInt(this.id);
		dest.writeInt(this.title);
		dest.writeInt(this.description);
		dest.writeByte((byte) (this.safe_for_work ? 0x01 : 0x00));
		dest.writeLong(this.downloads);
		dest.writeInt(this.link_to_app);
	}
	@Override
	public int compareTo(final Application a) {
		return (int) (a.downloads - this.downloads);
	}
	@Override
	public String toString() {
		
		final StringBuilder builder = new StringBuilder().append(this.title)
														 .append(": ").append(this.description)
														 .append(" (").append(this.link_to_app)
														 .append(")");
		
		return builder.toString();
	}
	
}
