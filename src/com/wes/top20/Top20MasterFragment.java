package com.wes.top20;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wes.top20.model.Application;
import com.wes.top20.model.ApplicationArray;

// TODO Substitute for a list fragment. This may be a better approach.
/** Fragment that loads the list view (master view) into the app. */
public final class Top20MasterFragment extends ListFragment {
	
	private final static class Top20ListAdapter extends ArrayAdapter<Application> {
		
		private ApplicationArray array;
		private LayoutInflater inflater;
		
		public Top20ListAdapter(final Context context, final LayoutInflater inflater) {
			
			super(context, R.layout.top20_list_item);
			
			this.inflater = inflater;
			this.array = ApplicationArray.getArray();
		}
		
		@Override
		public int getCount() {
			return 20;
		}
		@Override
		public Application getItem(final int position) {
			return this.array.getApp(position);
		}
		@Override
		public long getItemId(final int position) {
			return position;
		}
		@Override
		public View getView(final int position, View convertView, final ViewGroup parent) {
			
			final Application app = this.getItem(position);
			
			if (convertView == null)
				convertView = this.inflater.inflate(R.layout.top20_list_item, parent, false);
			
			// Set new data from app item.
			//Null pointer exception??? 
			((ImageView) (convertView.findViewById(R.id.app_image))).setImageResource(app.getAppIconReference());
			((TextView) (convertView.findViewById(R.id.title))).setText(app.getTitle());
			((TextView) (convertView.findViewById(R.id.downloads))).setText(app.getDownloads() + " downloads");
			
			return convertView;
		}
		
	}
	
	private Fragment details;
	
	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		
		super.onActivityCreated(savedInstanceState);
		this.setListAdapter(new Top20ListAdapter(this.getActivity(), this.getLayoutInflater(savedInstanceState)));
	}
	@Override
	public void onListItemClick(final ListView l, final View v, final int position, final long id) {
		
		if (this.details == null)
			this.details = new Top20DetailFragment();
		
		final Bundle b = new Bundle();
		b.putInt("start_position", position);
		this.details.setArguments(b);
		
		final FragmentManager manager = Top20MasterFragment.this.getFragmentManager();
		manager.beginTransaction()
			   .addToBackStack("master")
			   .replace(R.id.item_layout, this.details, "details")
			   .commit();
	}
	
}
