package com.wes.top20;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.wes.top20.model.ApplicationArray;

/** Entry point of application. */
public final class Top20 extends FragmentActivity {
	
	private Fragment master;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.top20_layout);
		
		ApplicationArray.getArray(); // This initializes the array so all load is performed before the user even notices it.
		
		final FragmentManager manager = this.getSupportFragmentManager();
		this.master = new Top20MasterFragment();
		
		manager.beginTransaction()
			   .add(R.id.item_layout, this.master, "master")
			   .commit();
	}
	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		
		final FragmentManager manager = this.getSupportFragmentManager();
		manager.beginTransaction()
			   .commit();
		
		super.onSaveInstanceState(outState);
	}
	
	
}
